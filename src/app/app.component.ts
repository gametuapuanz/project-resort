import { Component, ViewChild, HostListener, OnInit } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';
import { AddRoomComponent } from './components/add-room/add-room.component';
import { EditRoomComponent } from './components/edit-room/edit-room.component';
import { DeleteRoomComponent } from './components/delete-room/delete-room.component';
import { element } from 'protractor';
import { RouterLinkWithHref } from '@angular/router';

// modal
export interface roomElement {
  id: number;
  site: number;
  total: number;
  status: string;
}

// array
const ELEMENT_DATA: roomElement[] = [
  { id: 1, site: 3, total: 1200, status: 'เปิดใช้งาน' },
  { id: 2, site: 2, total: 1000, status: 'ปิดปรับปรุง' },
  { id: 3, site: 1, total: 700, status: 'เปิดใช้งาน' }

]
const lastId = 3;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'resort';
  id: number;
  site: string;
  total: number;
  status: string;
  constructor(private dialog: MatDialog) { }
  // หัวตาราง
  displayedColumns: string[] = ['id', 'site', 'total', 'status', 'tool'];

  // กำหนดข้อมูลในตางราง
  dataSource = ELEMENT_DATA;

  addRoom() {
    // เปิดหน้า dialog ( modal )
    const dialogRef = this.dialog.open(AddRoomComponent, {
      width: '500px',
      data: {
      }
    });
    // dialog ปิดแล้วให้ทำไรต่อ
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);

    });
  }
  editRoom() {
    // เปิดหน้า dialog ( modal )
    const dialogRef = this.dialog.open(EditRoomComponent, {
      width: '500px',
      data: {
      }
    });
    // dialog ปิดแล้วให้ทำไรต่อ
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }
  deleteRoom(row_obj) {
    // เปิดหน้า dialog ( modal )
    const dialogRef = this.dialog.open(DeleteRoomComponent, {
      width: '500px',
      data: {
        row_obj: []
      }
    });
    console.log(row_obj);



    // dialog ปิดแล้วให้ทำไรต่อ
    dialogRef.afterClosed().subscribe(result => {

      console.log(this.dataSource[row_obj]);
      if (row_obj = this.dataSource[result]) {
        this.dataSource.splice(result, 1);
      }
    });
    return this.dataSource
  }
}