import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

export interface roomElement {
  id: number;
  site: number;
  total: number;
  status: string;
}

@Component({
  selector: 'app-add-room',
  templateUrl: './add-room.component.html',
  styleUrls: ['./add-room.component.scss']
})
export class AddRoomComponent implements OnInit {
  constructor(
    // กำหนดให้หน้าที่เรียก Dialog ส่งข้อมูลมาได้
    @Inject(MAT_DIALOG_DATA) public data: any,
    // กำหนดตัวแปลเพื่อให้เรียกใช้ dialog
    private dialogRef: MatDialogRef<AddRoomComponent>, ) { }

  // เมื่อโหลด html เสร็จ ให้ component เรียก function หรือทำอะไรต่อ
  ngOnInit() {

  }

  onClose() {
    this.dialogRef.close();
  }

  onSave() {
    const value = {
      id: Number,
      site: String,
      total: Number,
      status: String
    };
    console.log(value);
    this.dialogRef.close(value);
  }

}
