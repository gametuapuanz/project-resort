import { element } from 'protractor';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';


export interface roomElement {
  id: number;
  site: number;
  total: number;
  status: string;
}

@Component({
  selector: 'app-delete-room',
  templateUrl: './delete-room.component.html',
  styleUrls: ['./delete-room.component.scss']
})

export class DeleteRoomComponent implements OnInit {

  constructor(
    // กำหนดให้หน้าที่เรียก Dialog ส่งข้อมูลมาได้
    @Inject(MAT_DIALOG_DATA) public data: any,
    // กำหนดตัวแปลเพื่อให้เรียกใช้ dialog
    private dialogRef: MatDialogRef<DeleteRoomComponent>, ) {

  }

  // เมื่อโหลด html เสร็จ ให้ component เรียก function หรือทำอะไรต่อ
  ngOnInit() {
    console.log(this.data);
  
  }

  onClose() {
    this.dialogRef.close();
  }

  onSave() {
    const result = {
    };
    this.dialogRef.close(result);
  }
}